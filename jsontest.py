import unittest
from scraper import write_json
from scraper import write_data
import json


class testJson(unittest.TestCase):

    def test_export_json(self):
        testDict = {'Yunes': 'cs242', "year": "1999"}
        write_json(testDict, 'testYunes.json')
        with open('testYunes.json') as f:
            file_data = json.load(f)
        self.assertEqual(file_data["Yunes"], 'cs242')
        self.assertEqual(file_data["year"], "1999")

    def test_import_json(self):
        testDict = {'Yunes': 'cs242', "year": "1999"}
        json.dump(testDict, open('testYunes.json', 'w'))
        file_data = write_data('testYunes.json')
        self.assertEqual(file_data["Yunes"], 'cs242')
        self.assertEqual(file_data["year"], "1999")
    
    def test_together(self):
        testDict = {'Yunes': 'cs242', "year": "1999"}
        write_json(testDict, 'testYunes.json')
        file_data = write_data('testYunes.json')
        self.assertEqual(file_data["Yunes"], 'cs242')
        self.assertEqual(file_data["year"], "1999")

    def test_none_data(self):
        testDict = {'Yunes': 'cs242', "year": "1999"}
        write_json(testDict, 'testYunes.json')
        file_data = write_data('testYunes.json')
        self.assertFalse('Tony' in file_data)

if __name__ == '__main__':
    unittest.main()