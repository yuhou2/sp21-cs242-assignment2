## Goodreads-Scraper
Goodreads is a website that collects information on books as well as reviews from the community.

## Programming Language
python

## Information
For author, I stored name: Name of the Author; author_url: the page URL; author_id: a unique identifier of the author; rating: the rating of the author; rating_count: the number of rating the author received; review_count: the number of comments the author received; image_url: A URL of the Author's image; related_authors: A list of Author related to the author; author_books: A list of books by the author

For books, I stored book_url: URL of the Book; title: Name of the Book; book_id: A unique identifier of the book; ISBN: the ISBN of the book; author_url: URL of the author of the book; author: Author of the book; rating: the rating of the book; rating_count: the number of rating the book received; review_count: the number of comments the book received;image_url: A URL of the book's image; similar_books: A list of books similar or related to the book

## Goodreads-API
An application interface that other people can access the database scraped from Goodreads

## Environment setup
•	Python 3.7.0
•	aniso8601==8.0.0
•	click==7.1.2
•	Flask==1.1.2
•	Jinja2==2.11.2
•	MarkupSafe==1.1.1
•	pytz==2020.1
•	six==1.15.0
•	Werkzeug==1.0.1
