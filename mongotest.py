import unittest
import pymongo
from pymongo import MongoClient
from dotenv import load_dotenv
import os
import ssl
load_dotenv()

def connect():
    DB_PASSWORD = os.getenv('PASSWORD')
    client = pymongo.MongoClient("mongodb+srv://YunesHou:" + DB_PASSWORD + "@cluster0.zwdte.mongodb.net/goodreads?retryWrites=true&w=majority", ssl_cert_reqs=ssl.CERT_NONE)
    return client

class testMongo(unittest.TestCase):
    def test_connect(self):
        client = connect()
        self.assertTrue(client)

    def test_author(self):
        client = connect()
        db = client["goodreads"]
        collection = db["author"]
        self.assertTrue(collection.count_documents({}) > 50)

    def test_book(self):
        client = connect()
        db = client["goodreads"]
        collection = db["book"]
        self.assertTrue(collection.count_documents({}) >= 200)


if __name__ == '__main__':
    unittest.main()


