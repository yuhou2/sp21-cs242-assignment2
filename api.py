import os
import json
import scraper
import convert_int
import argparse
import pymongo
from flask import Flask, request, Response, render_template, jsonify
from flask_pymongo import PyMongo
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)
DB_PASSWORD = os.getenv('PASSWORD')
MONGODBPRE = "mongodb+srv://YunesHou:"
MONGODBNAME = "@cluster0.zwdte.mongodb.net/goodreads?retryWrites=true&w=majority"
app.config['MONGO_DBNAME'] = "goodreads"
app.config['MONGO_URI'] = MONGODBPRE + DB_PASSWORD + MONGODBNAME

mongo = PyMongo(app)


def book_serializer(book):
    return {
        'book_url': book['book_url'],
        'title': book['title'],
        'book_id': book['book_id'],
        'ISBN': book['ISBN'],
        'author': book['author'],
        'author_url': book['author_url'],
        'rating': book['rating'],
        'rating_count': book['rating_count'],
        'review_count': book['review_count'],
        'image_url': book['image_url'],
        'similar_books': book['similar_books']
    }


def author_serializer(author):
    return {
        'name': author['name'],
        'author_url': author['author_url'],
        'author_id': author['author_id'],
        'rating': author['rating'],
        'rating_count': author['rating_count'],
        'review_count': author['review_count'],
        'image_url': author['image_url'],
        'related_authors': author['related_authors'],
        'author_books': author['author_books']
    }


@app.route('/allBooks', methods=['GET'])
def seeAllBooks():
    """Returns all Books"""
    book_entries = mongo.db.book
    all_books = list(book_entries.find().sort([('rating', pymongo.DESCENDING)]))
    all_books = jsonify([*map(book_serializer, all_books)])
    return all_books


@app.route('/allAuthors', methods=['GET'])
def seeAllAuthors():
    """Return all Authors"""
    author_entries = mongo.db.author
    all_authors = list(author_entries.find().sort([('rating', pymongo.DESCENDING)]))
    all_authors = jsonify([*map(author_serializer, all_authors)])
    return all_authors


@app.route('/search', methods=['GET'])
def search_data():
    """name represent book or author. attr represents the sttribute of book or author. value is the earch input"""
    if args.query is None:
        input_query = request.args['q']
        print(input_query)
    else:
        input_query = args.query.strip("'")
    if 'AND' in input_query:
        query1, query2 = input_query.split('AND')
        key, value1 = query1.split(':')
        name, attr1 = key.split('.')
        attr2, value2 = query2.split(':')
        if key is None or value1 is None or name is None or attr1 is None or attr2 is None or value2 is None:
            return call_response(msg="Invalid input", code=400)
        dic = search_and_helper(name, attr1, attr2, value1, value2)
    else:
        key, value = input_query.split(':')
        if key is None or value is None:
            return call_response(msg="Invalid input", code=400)
        name, attr = key.split('.')
        if name is None or attr is None:
            return call_response(msg="Invalid input", code=400)
        if 'OR' in value:
            dic = search_or_helper(name, attr, value)
        elif 'NOT' in value:
            dic = search_not_helper(name, attr, value)
        elif '>' in value:
            dic = search_gt_helper(name, attr, value)
        elif '<' in value:
            dic = search_lt_helper(name, attr, value)
        elif '"' in value:
            dic = search_quotation_helper(name, attr, value)
        else:
            dic = search_colon_helper(name, attr, value)
    print(dic)
    return dic


def search_quotation_helper(name, attr, value):
    """if the search query contain quotation"""
    find_value = value.strip('"')
    res = []
    dic = dict()
    if name == 'book':
        cur = mongo.db.book.find({attr: {'$regex': find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        cur = mongo.db.author.find({attr: {'$regex': find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_colon_helper(name, attr, value):
    """if the search query contain colon"""
    find_value = value
    res = []
    dic = dict()
    if name == 'book':
        cur = mongo.db.book.find({attr: {'$regex': '.*' + find_value + '.*'}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        cur = mongo.db.author.find({attr: {'$regex': '.*' + find_value + '.*'}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_or_helper(name, attr, value):
    """if the search query contain or"""
    value1, value2 = value.split('OR')
    value1 = value1.strip('"')
    value2 = value2.strip('"')
    res = []
    dic = dict()
    if name == 'book':
        cur = mongo.db.book.find({"$or": [{attr: {'$regex': value1}}, {attr: {'$regex': value2}}]})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        print(res)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        cur = mongo.db.author.find({"$or": [{attr: {'$regex': value1}}, {attr: {'$regex': value2}}]})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_and_helper(name, attr1, attr2, value1, value2):
    """if the search query contain and"""
    value1 = value1.strip('"')
    value2 = value2.strip('"')
    res = []
    dic = dict()
    if name == 'book':
        cur = mongo.db.book.find({"$and": [{attr1: {'$regex': value1}}, {attr2: {'$regex': value2}}]})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        print(res)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        cur = mongo.db.author.find({"$and": [{attr1: {'$regex': value1}}, {attr2: {'$regex': value2}}]})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_not_helper(name, attr, value):
    """if the search query contain not"""
    find_value = value.strip('NOT').strip('"')
    res = []
    dic = dict()
    if name == 'book':
        cur = mongo.db.book.find({attr: {"$not": {'$regex': find_value}}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        cur = mongo.db.author.find({attr: {"$not": {'$regex': find_value}}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_gt_helper(name, attr, value):
    """if the search query contain greater than"""
    find_value = float(value.strip('>'))
    print(find_value)
    res = []
    dic = dict()
    if name == 'book':
        convert_int.convert_book()
        cur = mongo.db.book.find({attr: {"$gt": find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        # print(res)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        convert_int.convert_author()
        cur = mongo.db.author.find({attr: {"$gt": find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


def search_lt_helper(name, attr, value):
    """if the search query contain less than"""
    find_value = float(value.strip('<'))
    print(find_value)
    res = []
    dic = dict()
    if name == 'book':
        convert_int.convert_book()
        cur = mongo.db.book.find({attr: {"$lt": find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        # print(res)
        output = [sub['title'] for sub in res]
        dic["book title"] = output
    else:
        convert_int.convert_author()
        cur = mongo.db.author.find({attr: {"$lt": find_value}})
        if cur is None:
            return call_response(msg="No entry was found that matches query", code=400)
        for i in cur:
            res.append(i)
        output = [sub['name'] for sub in res]
        dic["author name"] = output
    return dic


@app.route('/book', methods=['GET'])
def get_book_data():
    """Get data for books."""
    output = dict()
    if args.book_id is None:
        id_value = request.args['book_id'].strip('"')
    else:
        id_value = args.book_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    books_id = mongo.db.book.find({'book_id': {'$regex': id_value}})
    if books_id:
        for book in books_id:
            output['book_url'] = book['book_url']
            output['title'] = book['title']
            output['book_id'] = book['book_id']
            output['ISBN'] = book['ISBN']
            output['author'] = book['author']
            output['author_url'] = book['author_url']
            output['rating'] = book['rating']
            output['rating_count'] = book['rating_count']
            output['review_count'] = book['review_count']
            output['image_url'] = book['image_url']
            output['similar_books'] = book['similar_books']
    else:
        return call_response(msg="No Entries Found", code=400)
    print(output)
    return output


@app.route('/author', methods=['GET'])
def get_author_data():
    """Get data for authors"""
    output = dict()
    if args.author_id is None:
        id_value = request.args['author_id'].strip('"')
    else:
        id_value = args.author_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    authors_id = mongo.db.author.find({'author_id': {'$regex': id_value}})
    if authors_id:
        for author in authors_id:
            output['name'] = author['name']
            output['author_url'] = author['author_url']
            output['author_id'] = author['author_id']
            output['rating'] = author['rating']
            output['rating_count'] = author['rating_count']
            output['review_count'] = author['review_count']
            output['image_url'] = author['image_url']
            output['related_authors'] = author['related_authors']
            output['author_books'] = author['author_books']
    else:
        return call_response(msg="No Entries Found", code=400)
    print(output)
    return output


@app.route('/book', methods=['DELETE'])
def delete_book():
    """Delete data for books"""
    if args.book_id is None:
        id_value = request.args['book_id'].strip('"')
    else:
        id_value = args.book_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    elem_to_delete = mongo.db.book.find_one({'book_id': {'$regex': id_value}})
    
    if elem_to_delete is None:
        return call_response(msg="No entry was found that matches query", code=400)
    mongo.db.book.delete_one(elem_to_delete)
    print("Book has been deleted")
    return call_response(msg="Book has been deleted", code=200)


@app.route('/author', methods=['DELETE'])
def delete_author():
    """Delete data for authors"""
    if args.author_id is None:
        id_value = request.args['author_id'].strip('"')
    else:
        id_value = args.author_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    elem_to_delete = mongo.db.author.find_one({'author_id': {'$regex': id_value}})
    if elem_to_delete is None:
        return call_response(msg="No entry was found that matches query", code=400)
    mongo.db.author.delete_one(elem_to_delete)
    print("Author has been deleted")
    return call_response(msg="Author has been deleted", code=200)


@app.route('/book', methods=['PUT'])
def update_book():
    """Update data for books"""
    if args.json_list is None:
        data = json.loads(request.data)['dict']
    else:
        data = json.loads(args.json_list)
    if args.book_id is None:
        id_value = request.args['book_id'].strip('"')
    else:
        id_value = args.book_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    element_to_update = mongo.db.book.find_one({'book_id': {'$regex': id_value}})
    if element_to_update is None:
        return call_response(msg="No entry was found that matches query", code=400)
    new_values = {"$set": data}
    mongo.db.book.update_one(element_to_update, new_values, upsert=False)
    update_result = mongo.db.book.find_one({'book_id': {'$regex': id_value}})
    print(update_result)
    return call_response(msg="Book has been updated", code=200)
    

@app.route('/author', methods=['PUT'])
def update_author():
    """Update data for authors"""
    if args.json_list is None:
        data = json.loads(request.data)
    else:
        data = json.loads(args.json_list)
    if args.author_id is None:
        id_value = request.args['author_id'].strip('"')
    else:
        id_value = args.author_id.strip('"')
    if len(id_value) <= 2:
        return call_response(msg="Must enter characters", code=400)
    element_to_update = mongo.db.author.find_one({'author_id': {'$regex': id_value}})
    if element_to_update is None:
        return call_response(msg="No entry was found that matches query", code=400)
    new_values = {"$set": data}
    mongo.db.author.update_one(element_to_update, new_values, upsert=False)
    update_result = mongo.db.author.find_one({'book_id': {'$regex': id_value}})
    print(update_result)
    return call_response(msg="Author has been updated", code=200)


@app.route('/book', methods=['POST'])
def post_book_data():
    """Post data for books and sets values to None if values are not provided"""
    if args.infile is None:
        data = json.loads(request.data)['postBook']
    else:
        data = json.load(args.infile[0])
    print(data)
    if data is None or data == {} or all(value == '' for value in data.values()):
        return call_response(msg="Please Enter Information", code=400)
    data.get('book_url', None)
    data.get('title', None)
    data.get('book_id', None)
    data.get('ISBN', None)
    data.get('author', None)
    data.get('author_url', None)
    data.get('rating', None)
    data.get('rating_count', None)
    data.get('review_count', None)
    data.get('image_url', None)
    data.get('similar_books', None)
    if isinstance(data, list):
        mongo.db.book.insert_many(data)
    else:
        mongo.db.book.insert_one(data)
    # output = [sub['title'] for sub in data]
    # book_dic = dict()
    # book_dic["book title"] = output
    
    id_value = data['book_id']
    update_result = mongo.db.book.find_one({'book_id': {'$regex': id_value}})
    print(update_result)

    return call_response(msg="Book has been add to collection", code=200)


@app.route('/author', methods=['POST'])
def post_author_data():
    """Post data for authors"""
    if args.infile is None:
        data = json.loads(request.data)['postAuthor']
    else:
        data = json.load(args.infile[0])
    print(data)
    if data is None or data == {} or all(value == '' for value in data.values()):
        return call_response(msg="Please Enter Information", code=400)
    data.get('name', None)
    data.get('author_url', None)
    data.get('author_id', None)
    data.get('rating', None)
    data.get('rating_count', None)
    data.get('review_count', None)
    data.get('image_url', None)
    data.get('related_authors', None)
    data.get('author_books', None)
    if isinstance(data, list):
        mongo.db.author.insert_many(data)
    else:
        mongo.db.author.insert_one(data)
    
    print(data)
    return call_response(msg="Author has been add to collection", code=200)


@app.route('/scrape/book', methods=['POST'])
def scrape_book():
    """Scrape data for books by input url"""
    if args.url is None:
        url_value = request.args['book_url'].strip('"')
    else:
        url_value = args.url.strip('"')
    print(url_value)
    list_of_books = scraper.get_all_books_with_information(url_value, 1)
    for book_url in list(list_of_books):
        scraper.scrape_book_info(book_url)
    scraper.write_json(scraper.BOOK_JSONS, "books.json")

    with open('books.json') as f_books:
        book_data = json.load(f_books)
    if isinstance(book_data, list):
        mongo.db.book.insert_many(book_data)
    else:
        mongo.db.book.insert_one(book_data)
    output = [sub['title'] for sub in book_data]
    book_dic = dict()
    book_dic["book title"] = output
    return call_response(msg="Book has been scraped and add them to collection", code=200)


@app.route('/scrape/author', methods=['POST'])
def scrape_author():
    """Scrape data for authors by input url"""
    if args.url is None:
        url_value = request.args['author_url'].strip('"')
    else:
        url_value = args.url.strip('"')
    print(url_value)
    list_of_authors = scraper.get_all_authors_with_information(url_value, 1)
    for author_url in list(list_of_authors):
        scraper.scrape_author_info(author_url)
    scraper.write_json(scraper.AUTHOR_JSONS, "authors.json")
    with open('authors.json') as f_authors:
        author_data = json.load(f_authors)
    if isinstance(author_data, list):
        mongo.db.author.insert_many(author_data)
    else:
        mongo.db.author.insert_one(author_data)
    output = [sub['name'] for sub in author_data]
    author_dic = dict()
    author_dic["author name"] = output
    return call_response(msg="Author has been scraped and add them to collection", code=200)


def call_response(msg, code):
    """return the response with messages and code"""
    return Response(response=json.dumps({"message": msg}), status=code, mimetype="application/json")


parser = argparse.ArgumentParser(description='API server')
parser.add_argument('-bi', '--book_id', type=str,
                    help='book_id for get, put and delete book information')
parser.add_argument('-ai', '--author_id', type=str,
                    help='author_id for get, put and delete author information')
parser.add_argument('-query', '--query', type=str,
                    help='query for get author or book infromation')
parser.add_argument('-json_list', '--json_list', type=str,
                    help='json_list for update book or author information')
parser.add_argument('-url', '--url', type=str,
                    help='The url start to scrape')
parser.add_argument('-f', '--infile', nargs=1,
                    help="JSON file to be processed",
                    type=argparse.FileType('r'))
parser.add_argument('-na', '--numberofauthors', type=int,
                    help='Number of authors user want to input', default=5)
parser.add_argument('-nb', '--numberofbooks', type=int,
                    help='Number of books user want to input', default=10)

group = parser.add_mutually_exclusive_group()
group.add_argument('-gb', '--getbook', action='store_true', help='get book information')
group.add_argument('-ga', '--getauthor', action='store_true', help='get author information')
group.add_argument('-putb', '--putbook', action='store_true', help='update book information')
group.add_argument('-puta', '--putauthor', action='store_true', help='update author information')
group.add_argument('-postb', '--postbook', action='store_true', help='add new book information')
group.add_argument('-posta', '--postauthor', action='store_true', help='add new author information')
group.add_argument('-db', '--deletebook', action='store_true', help='delete book information')
group.add_argument('-da', '--deleteauthor', action='store_true', help='delete author information')
group.add_argument('-search', '--search', action='store_true', help='search information by query')
group.add_argument('-sb', '--scrapebook', action='store_true', help='scrape book information')
group.add_argument('-sa', '--scrapeauthor', action='store_true', help='scrape author information')
args = parser.parse_args()

if __name__ == '__main__':
    if args.getbook:
        get_book_data()
    elif args.getauthor:
        get_author_data()
    elif args.putbook:
        update_book()
    elif args.putauthor:
        update_author()
    elif args.postbook:
        post_book_data()
    elif args.postauthor:
        post_author_data()
    elif args.deletebook:
        delete_book()
    elif args.deleteauthor:
        delete_author()
    elif args.search:
        search_data()
    elif args.scrapebook:
        scrape_book()
    elif args.scrapeauthor:
        scrape_author()
    else:
        app.run(debug=True)
