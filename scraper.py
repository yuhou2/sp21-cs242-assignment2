import json
import random
import os
import ssl
import pymongo
import requests
import argparse
from flask import request
from bs4 import BeautifulSoup
from dotenv import load_dotenv
load_dotenv()
ssl._create_default_https_context = ssl._create_unverified_context


DB_PASSWORD = os.getenv('PASSWORD')

MONGODBPRE = "mongodb+srv://YunesHou:"
MONGODBNAME = "@cluster0.zwdte.mongodb.net/goodreads?retryWrites=true&w=majority"
CLIENT = pymongo.MongoClient(
    MONGODBPRE + DB_PASSWORD + MONGODBNAME,
    ssl_cert_reqs=ssl.CERT_NONE)
DB = CLIENT.goodreads
AUTHOR_COLLECTION = DB["author"]
BOOK_COLLECTION = DB["book"]


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, complex):
            return [obj.real, obj.imag]
        return json.JSONEncoder.default(self, obj)


AUTHOR_JSONS = []
BOOK_JSONS = []

# For Authors, you need to store:
# name	Name of the Author
# author_url	the page URL
# author_id	a unique identifier of the author
# rating	the rating of the author
# rating_count	the number of rating the author received
# review_count	the number of comments the author received
# image_url	A URL of the Author's image
# related_authors	A list of Author related to the author
# author_books	A list of books by the author

# Collect authors' information


def get_author_url(url):
    """Get the author url"""
    headers = {'User-Agent': 'User Agent16', }
    page = requests.get(url, headers=headers).text
    soup = BeautifulSoup(page, 'lxml')
    return soup.find('div', class_='authorName__container').a.get('href')


def get_all_author_books(author_soup, author_info):
    """Get all books from authors"""
    try:
        books = (author_soup.find('table', class_="stacked tableList")).find_all('td', width="100%")
        author_books = []
        for book in books:
            author_books.append(((book.find('a', class_="bookTitle").text).lstrip("\n")).rstrip("\n"))
        author_info['author_books'] = author_books
    except AttributeError:
        author_info['author_books'] = None
    return author_books


def scrape_similar_authors(url, author_info):
    """Get similar authors"""
    headers = {'User-Agent': 'User Agent16', }
    url = 'https://www.goodreads.com'+url
    similar_author_page = requests.get(url, headers=headers).text
    soup = BeautifulSoup(similar_author_page, 'lxml')
    similar_authors = soup.find_all('div', class_="listWithDividers__item")
    related_author_info = []
    for item in similar_authors:
        related_author_info.append(item.a.get('href'))
    author_info['related_authors'] = related_author_info
    author_info['author_url'] = related_author_info[0]
    author_info['author_id'] = ''.join(filter(str.isdigit, related_author_info[0]))


def scrape_author_info(url):
    """Get authors information"""
    headers = {'User-Agent': 'User Agent16', }
    author_page = requests.get(url, headers=headers).text
    soup = BeautifulSoup(author_page, 'lxml')
    author_info = dict()
    try:
        author_info['name'] = (soup.find('h1', class_="authorName").text).strip('\n')
    except AttributeError:
        author_info['name'] = None
    try:
        metadata = soup.find('div', class_="hreview-aggregate")
        author_info['rating'] = metadata.find('span', class_="average").text
        author_info['rating_count'] = (metadata.find('span', class_="votes").text).strip().strip('\n')
        author_info['review_count'] = (metadata.find('span', class_="count").text).strip().strip('\n')
    except AttributeError:
        author_info['rating'] = None
        author_info['rating_count'] = None
        author_info['review_count'] = None
    try:
        author_info['image_url'] = (soup.find('div', class_="leftContainer authorLeftContainer")).img.get('src')
    except AttributeError:
        author_info['image_url'] = None
    authorbooks = get_all_author_books(soup, author_info)
    author_meta = soup.find('div', class_="hreview-aggregate")
    try:
        scrape_similar_authors((author_meta.find_all('a'))[1].get('href'), author_info)
    except IndexError:
        author_info['related_authors'] = None
        author_info['author_url'] = None
        author_info['author_id'] = None
    except AttributeError:
        author_info['related_authors'] = None
        author_info['author_url'] = None
        author_info['author_id'] = None
    add_to_list_author(author_info)
    return authorbooks


def add_to_list_author(author_info):
    """convert from dictionary type to a list"""
    author_object = {
        'name': author_info.get('name'),
        'author_url': author_info.get('author_url'),
        'author_id': author_info.get('author_id'),
        'rating': author_info.get('rating'),
        'rating_count': author_info.get('rating_count'),
        'review_count': author_info.get('review_count'),
        'image_url': author_info.get('image_url'),
        'related_authors': author_info.get('related_authors'),
        'author_books': author_info.get('author_books')
    }
    AUTHOR_JSONS.append(author_object)


def get_similar_authors_page(url):
    """ Gets the similar authors page url """
    headers = {'User-Agent': 'User Agent16', }
    similar_page = requests.get(url, headers=headers).text
    soup = BeautifulSoup(similar_page, 'lxml')
    related_authors = soup.find('div', class_="hreview-aggregate").find_all('a')
    return related_authors[1].get('href')


def get_total_authors(url):
    """Gets all the urls for author pages"""
    page = requests.get('https://www.goodreads.com'+url).text
    soup = BeautifulSoup(page, 'lxml')
    similar_authors = soup.find_all('div', class_="listWithDividers__item")
    authorlist = list()
    for item in similar_authors:
        authorlist.append(item.a.get('href'))
    return authorlist


def get_all_authors_with_information(url, numberofauthor):
    """ Gets all authors with count minimum """
    author_set = set()
    all_authors = get_total_authors(get_similar_authors_page(get_author_url(url)))
    for author_link in all_authors:
        author_set.add(author_link)
    while len(author_set) < int(numberofauthor):
        similar_authors = get_similar_authors_page(list(author_set)[random.randint(0, len(author_set) - 1)])
        for author_link in get_total_authors(similar_authors):
            author_set.add(author_link)
            print(str(len(author_set)) + ' Authors Found')
        
    return author_set


# For Books, you need to store:
# book_url	URL of the Book
# title Name of the Book
# book_id	A unique identifier of the book
# ISBN	the ISBN of the book
# author_url	URL of the author of the book
# author	Author of the book
# rating	the rating of the book
# rating_count	the number of rating the book received
# review_count	the number of comments the book received
# image_url	A URL of the book's image
# similar_books	A list of books similar or related to the book


def get_related_books(soup, book_info):
    """Gets all similar books in a list."""
    try:
        related_list = soup.find_all('li', class_="cover")
        related_list_books = []
        for book in related_list:
            related_list_books.append(book.a.get('href'))
        book_info['similar_books'] = related_list_books
        return related_list
    except AttributeError:
        book_info['similar_books'] = None


def scrape_book_info(url):
    """Gets book's information"""
    headers = {'User-Agent': 'User Agent16', }
    page = requests.get(url, headers=headers).text
    soup = BeautifulSoup(page, 'lxml')
    book_info = dict()
    book_info['book_url'] = url
    book_info['book_id'] = ''.join(filter(str.isdigit, url))
    try:
        book_info['title'] = (soup.find(id='bookTitle').text).strip().strip('\n')
    except AttributeError:
        book_info['title'] = None
    try:
        book_info['author'] = ((soup.find('div', class_='authorName__container')).text).strip('\n')
        book_info['author_url'] = (soup.find('div', class_='authorName__container')).a.get('href')
    except AttributeError:
        book_info['author'] = None
        book_info['author_url'] = None
    try:
        metadata = soup.find('div', id='bookMeta')
        book_info['rating'] = (metadata.find('span', itemprop="ratingValue").text).strip().strip('\n')
    except AttributeError:
        book_info['rating'] = None

    try:
        metadata = soup.find('div', id='bookMeta')
        review = metadata.find_all('a', class_="gr-hyperlink")
        book_info['rating_count'] = ''.join(filter(str.isdigit, review[0].text))
        book_info['review_count'] = ''.join(filter(str.isdigit, review[1].text))
    except AttributeError:
        book_info['rating_count'] = None
        book_info['review_count'] = None

    try:
        book_info['image_url'] = (soup.find('div', class_="bookCoverPrimary")).a.get('href')
    except AttributeError:
        book_info['image_url'] = None

    try:
        isbn = soup.find_all('div', class_="clearFloats")
        isbn = (isbn[1].find('div', class_="infoBoxRowItem").text).strip().strip('\n').replace(" ", '').replace("\n", "")
        if len(isbn) < 10:
            book_info['ISBN'] = None
        else:
            book_info['ISBN'] = isbn[0:10]
    except AttributeError:
        book_info['ISBN'] = None
    except IndexError:
        book_info['ISBN'] = None

    related_books = get_related_books(soup, book_info)
    add_to_list_book(book_info)
    return related_books


def add_to_list_book(book_info):
    """convert from dictionary type to a list"""
    book_object = {
        'book_url': book_info.get('book_url'),
        'title': book_info.get('title'),
        'book_id': book_info.get('book_id'),
        'ISBN': book_info.get('ISBN'),
        'author': book_info.get('author'),
        'author_url': book_info.get('author_url'),
        'rating': book_info.get('rating'),
        'rating_count': book_info.get('rating_count'),
        'review_count': book_info.get('review_count'),
        'image_url': book_info.get('image_url'),
        'similar_books': book_info.get('similar_books')
    }
    BOOK_JSONS.append(book_object)


def get_total_books(url):
    """Gets all the urls for author pages"""
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'lxml')
    related_lists = soup.find_all('li', class_="cover")
    total_books = list()
    for book in related_lists:
        total_books.append(book.a.get('href'))
    return total_books


def get_all_books_with_information(url, numberofbook):
    """ Gets all books with count minimum """
    book_set = set()
    all_books = get_total_books(url)
    for book_link in all_books:
        book_set.add(book_link)
    while len(book_set) < int(numberofbook):
        all_books = get_total_books(list(book_set)[random.randint(0, len(book_set) - 1)])
        for book_link in all_books:
            book_set.add(book_link)
        print(str(len(book_set)) + " Books Found")
    return book_set


def store_data():
    """store the data into mongodb database"""
    with open('authors.json') as f_authors:
        author_data = json.load(f_authors)
    if isinstance(author_data, list):
        AUTHOR_COLLECTION.insert_many(author_data)
    else:
        AUTHOR_COLLECTION.insert_one(author_data)
    print('Authors have been added to database')
    with open('books.json') as f_books:
        book_data = json.load(f_books)
    if isinstance(book_data, list):
        BOOK_COLLECTION.insert_many(book_data)
    else:
        BOOK_COLLECTION.insert_one(book_data)
    print('Books have been added to database')


def update_author():
    """Update data for books"""
    key = list(request.args.keys())[0]
    val = request.args[key].strip('"')
    data = request.get_json()
    keyvalue = {key: val}
    CLIENT.goodreads.author.update_one(keyvalue, {'$set': data})


def update_book():
    """Update data for books"""
    key = list(request.args.keys())[0]
    value = request.args[key].strip('"')
    data = request.get_json()
    keyvalue = {key: value}
    CLIENT.goodreads.book.update_one(keyvalue, {"$set": data})


def write_json(json_list, input_file):
    """write into json"""
    with open(input_file, "w") as newfile:
        json.dump(json_list, newfile, cls=Encoder, indent=4)
        newfile.close()


def write_data(jsonfile):
    """get data from json"""
    with open(jsonfile) as json_file:
        data = json.load(json_file)
    return data


def update_db_from_json(inputjson):
    """if the input is a json file, then update the database with the json"""
    try:
        data = json.loads(inputjson)
        if len(data) == 9:
            if isinstance(data, list):
                AUTHOR_COLLECTION.insert_many(data)
            else:
                AUTHOR_COLLECTION.insert_one(data)
            print('Authors have been added to database')
        elif len(data) == 11:
            if isinstance(data, list):
                BOOK_COLLECTION.insert_many(data)
            else:
                BOOK_COLLECTION.insert_one(data)
            print('Books have been added to database')
        else:
            print('Invalid json file')
    except AttributeError:
        print("String could not be converted to JSON")


def main():
    """main function"""
    parser = argparse.ArgumentParser(description='Scraper Information')
    parser.add_argument('-na', '--numberofauthors', type=int,
                        help='Number of authors user want to input')
    parser.add_argument('-nb', '--numberofbooks', type=int,
                        help='Number of books user want to input')
    parser.add_argument('-url', type=str,
                        help='The url start to scrape')
    parser.add_argument('--infile', nargs=1,
                        help="JSON file to be processed",
                        type=argparse.FileType('r'))
    args = parser.parse_args()
    if args.infile:
        update_db_from_json(args.infile)
    else:
        if args.numberofbooks > 200:
            print('Warning: the number of book is large and it will run slowly')
        if args.numberofauthors > 50:
            print('Warning: the number of author is large and it will run slowly')
        global BOOK_JSONS
        global AUTHOR_JSONS
        print('getting Authors')
        list_of_authors = get_all_authors_with_information(args.url, args.numberofauthors)
        print('getting Books')
        list_of_books = get_all_books_with_information(args.url, args.numberofbooks)
        print('Scraping authors information')
        for author_url in list(list_of_authors):
            scrape_author_info(author_url)
        print('Scraping books information')
        for book_url in list(list_of_books):
            scrape_book_info(book_url)
        print("Converting Collected Data to JSON files")
        write_json(AUTHOR_JSONS, "authors.json")
        write_json(BOOK_JSONS, "books.json")
        store_data()


if __name__ == "__main__":
    main()
