import React from 'react';


export const AuthorCard = ({ result }) => {
    return (
     <>
      {result.map(author => {
          return(
              <ul key={author.author_id}>
                  <li>{author.name}</li>
              </ul>
          )
      })}
      </>
    )
   }