import React from 'react';


export const BookCard = ({ result }) => {
    return (
     <>
      {result.map(book => {
          return(
              <ul key={book.book_id}>
                  <li>{book.title}</li>
              </ul>
          )
      })}
      </>
    )
   }