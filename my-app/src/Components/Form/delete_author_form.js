import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const DeleteAuthorForm = ()=> {
    const [result, setResult] = useState([])
    const [deleteAuthor, setDeleteAuthor] = useState('')

    const handleChange = (event) => {
        setDeleteAuthor(event.target.value)
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/author?author_id=${deleteAuthor}`, {
            method: 'DELETE'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Type in author id to delete specific author
                    <input type="text" placeholder="Author ID" name="author_id" required value={deleteAuthor} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <Card result={result}/>
    </>)
}