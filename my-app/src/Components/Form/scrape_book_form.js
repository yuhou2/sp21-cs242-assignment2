import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const ScrapeBookForm = ()=> {
    const [result, setResult] = useState([])
    const [scrapeBook, setScrapeBook] = useState('')

    function handleChange(evt) {
        const value = evt.target.value;
        setScrapeBook(value);
      }
    
    function handleSubmit(event) {
        event.preventDefault()
        fetch(`/scrape/book?book_url=${scrapeBook}`, {
            method: 'POST'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="URL" name="url" value={scrapeBook} onChange={handleChange}></input>
                <input type="submit"></input>
            </form>
        </div>
        <Card result={result}/>
    </>)
}