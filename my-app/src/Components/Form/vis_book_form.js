import React,{ useState } from 'react';
import * as d3 from "d3";

export const VisBookForm = ()=> {
    const [k, setK] = useState([])

    const handleChange = (event) => {
        setK(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/allBooks`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => componentDidMount(data))
    }

    const drawChart = (data) => {
        var bookTitle = []
        var bookRate = []

        for(var j = 0; j < k; j++) {
            bookTitle[j] = data[j].title + " : " + data[j].rating
            bookRate[j] = data[j].rating
        }

        console.log(bookTitle)
        console.log(bookRate)

        const svg = d3.select("body")
            .append("svg")
            .attr("width", 500)
            .attr("height", 10000)
            .style("margin-left", 100);

        svg.selectAll("rect")
            .data(bookRate)
            .enter()
            .append("rect")
            .attr("x", (d, i) => 200 - 20 * d)
            .attr("y", (d, i) => 25 + i * 100)
            .attr("width", (d, i) => d * 20)
            .attr("height", 65)
            .attr("fill", "pink");

        svg.selectAll("text")
            .data(bookTitle)
            .enter()
            .append("text")
            .text((d) => d)
            .attr("x", (d, i) => 10)
            .attr("y", (d, i) => 13 + i * 100);
            
            
    }

    const componentDidMount = (data) => {
        console.log(data)
        drawChart(data);
    }

    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Top k Books
                    <input type="text" pattern="[0-9]*" placeholder="k" name="k" required value={k} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
    </>)
}