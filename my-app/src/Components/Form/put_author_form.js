import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const PutAuthorForm = ()=> {
    const [result, setResult] = useState([])
    const [putAuthor, setPutAuthor] = useState({'attribute':'', 'value':'', 'author_id':''})

    function handleChange(evt) {
        const value = evt.target.value;
        setPutAuthor({
          ...putAuthor,
          [evt.target.name]: value
        });
      }
    
    function handleSubmit(event) {
        event.preventDefault()
        
        const attr = putAuthor.attribute
        const val = putAuthor.value
        const dict = {}
        dict[attr] = val

        fetch(`/author?author_id=${putAuthor.author_id}`, {
            method: 'PUT',
            body: JSON.stringify({
                dict
            })
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    <h4> Select attribute </h4>
                    <select name="attribute" onChange={handleChange} value={putAuthor.attribute}>
                        <option value="name">Name</option>
                        <option value="author_url">Author URL</option>
                        <option value="rating">Rating</option>
                        <option value="rating_count">Rating Count</option>
                        <option value="review_count">Review Count</option>
                        <option value="image_url">Image URL</option>
                        <option value="related_authors">Related Authors</option>
                        <option value="author_books">Author Books</option>
                    </select>
                </label>
                <h4> Type in value to update </h4>
                <input type="text" placeholder="Value" name="value" onChange={handleChange} value={putAuthor.value}/>
                <h4> Type in author ID </h4>
                <input type="text" placeholder="Author ID" name="author_id" value={putAuthor.author_id} onChange={handleChange}></input>
                <input type="submit"></input>
            </form>
        </div>
        <Card result={result}/>
    </>)
}