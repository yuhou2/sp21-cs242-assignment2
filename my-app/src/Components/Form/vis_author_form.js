import React,{ useState } from 'react';
import * as d3 from "d3";

export const VisAuthorForm = ()=> {
    const [k, setK] = useState([])

    const handleChange = (event) => {
        setK(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/allAuthors`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => componentDidMount(data))
    }

    const drawChart = (data) => {
        var authorName = []
        var authorRate = []

        for(var j = 0; j < k; j++) {
            authorName[j] = data[j].name + " : " + data[j].rating
            authorRate[j] = data[j].rating
        }

        console.log(authorName)
        console.log(authorRate)

        const svg = d3.select("body")
            .append("svg")
            .attr("width", 500)
            .attr("height", 10000)
            .style("margin-left", 100);

        svg.selectAll("rect")
            .data(authorRate)
            .enter()
            .append("rect")
            .attr("x", (d, i) => 200 - 20 * d)
            .attr("y", (d, i) => 25 + i * 100)
            .attr("width", (d, i) => d * 20)
            .attr("height", 65)
            .attr("fill", "pink");

        svg.selectAll("text")
            .data(authorName)
            .enter()
            .append("text")
            .text((d) => d)
            .attr("x", (d, i) => 10)
            .attr("y", (d, i) => 13 + i * 100);
            
            
    }

    const componentDidMount = (data) => {
        drawChart(data);
    }

    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Top k Authors
                    <input type="text" pattern="[0-9]*" placeholder="k" name="k" required value={k} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
    </>)
}