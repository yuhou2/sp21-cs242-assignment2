import React,{useState, useEffect} from 'react';
import { BookCard } from '../Card/book_card';


export const ShowBookForm = ()=> {
    const [result, setResult] = useState([])

    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/allBooks`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    See All Books
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <BookCard result={result}/>
    </>)
}