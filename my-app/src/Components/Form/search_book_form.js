import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const SearchBookForm = ()=> {
    const [result, setResult] = useState([])
    const [searchBook, setSearchBook] = useState('')

    function handleChange(evt) {
        const value = evt.target.value;
        setSearchBook(value);
      }
    
    function handleSubmit(event) {
        event.preventDefault()
        fetch(`/search?q=${searchBook}`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Query" name="query" value={searchBook} onChange={handleChange}></input>
                <input type="submit"></input>
            </form>
        </div>
        <Card result={result}/>
    </>)
}