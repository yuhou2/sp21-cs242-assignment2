import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const GetBookForm = ()=> {
    const [result, setResult] = useState([])
    const [getBook, setGetBook] = useState('')

    const handleChange = (event) => {
        setGetBook(event.target.value)
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/book?book_id=${getBook}`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Type in book id to get book information
                    <input type="text" placeholder="Book ID" name="book_id" required value={getBook} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <Card result={result}/>
    </>)
}
