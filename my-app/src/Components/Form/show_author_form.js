import React,{ useState } from 'react';
import { AuthorCard } from '../Card/author_card';


export const ShowAuthorForm = ()=> {
    const [result, setResult] = useState([])

    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/allAuthors`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    See All Authors
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <AuthorCard result={result}/>
    </>)
}