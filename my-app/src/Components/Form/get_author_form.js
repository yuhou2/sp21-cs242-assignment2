import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const GetAuthorForm = ()=> {
    const [result, setResult] = useState([])
    const [getAuthor, setGetAuthor] = useState('')

    const handleChange = (event) => {
        setGetAuthor(event.target.value)
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/author?author_id=${getAuthor}`, {
            method: 'GET'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Type in author id to get author information
                    <input type="text" placeholder="Author ID" name="author_id" required value={getAuthor} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <Card result={result}/>
    </>)
}
