import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const DeleteBookForm = ()=> {
    const [result, setResult] = useState([])
    const [deleteBook, setDeleteBook] = useState('')

    const handleChange = (event) => {
        setDeleteBook(event.target.value)
    }
    const handleSubmit = (event) => {
        event.preventDefault()
        fetch(`/book?book_id=${deleteBook}`, {
            method: 'DELETE'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Type in book id to delete specific book
                    <input type="text" placeholder="Book ID" name="book_id" required value={deleteBook} onChange={handleChange}></input>
                    <input type="submit"></input>
                </label>
            </form>
        </div>
        <Card result={result}/>
    </>)
}
