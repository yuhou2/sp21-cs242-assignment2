import React,{useState, useEffect} from 'react';
import { Card } from '../Card/card';


export const ScrapeAuthorForm = ()=> {
    const [result, setResult] = useState([])
    const [scrapeAuthor, setScrapeAuthor] = useState('')

    function handleChange(evt) {
        const value = evt.target.value;
        setScrapeAuthor(value);
      }
    
    function handleSubmit(event) {
        event.preventDefault()
        fetch(`/scrape/author?author_url=${scrapeAuthor}`, {
            method: 'POST'
        }).then(response => response.json())
          .then(data => setResult(data))
    }
    return(
    <>
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="URL" name="url" value={scrapeAuthor} onChange={handleChange}></input>
                <input type="submit"></input>
            </form>
        </div>
        <Card result={result}/>
    </>)
}