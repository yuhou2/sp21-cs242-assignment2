import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import { GetBookForm } from './Components/Form/get_book_form';
import { GetAuthorForm } from './Components/Form/get_author_form';
import { ShowBookForm } from './Components/Form/show_book_form';
import { ShowAuthorForm } from './Components/Form/show_author_form';
import { DeleteBookForm } from './Components/Form/delete_book_form';
import { DeleteAuthorForm } from './Components/Form/delete_author_form';
import { PostBookForm } from './Components/Form/post_book_form';
import { PostAuthorForm } from './Components/Form/post_author_form';
import { PutBookForm } from './Components/Form/put_book_form';
import { PutAuthorForm } from './Components/Form/put_author_form';
import { SearchBookForm } from './Components/Form/search_book_form';
import { ScrapeBookForm } from './Components/Form/scrape_book_form';
import { ScrapeAuthorForm } from './Components/Form/scrape_author_form';
import { VisBookForm } from './Components/Form/vis_book_form';
import { VisAuthorForm } from './Components/Form/vis_author_form';
import NavbarItem from './Components/navbar';


function App() {
  return (
   <div className='App'>
    <NavbarItem/>
      <Switch>
        <Route path='/search'>
          <p1>Search</p1>
          <SearchBookForm/>
        </Route>
        <Route path='/get'>
          <span> </span>
          <h2> BOOK </h2>
          <GetBookForm/>
          <ShowBookForm/>
          <h2> AUTHOR </h2>
          <GetAuthorForm/>
          <ShowAuthorForm/>
        </Route>
        <Route path='/post'>
          <h2> BOOK </h2>
          <PostBookForm/>
          <h2> AUTHOR </h2>
          <PostAuthorForm/>
        </Route>
        <Route path='/delete'>
          <span> </span>
          <h2> BOOK </h2>
          <DeleteBookForm/>
          <h2> AUTHOR </h2>
          <DeleteAuthorForm/>
        </Route>
        <Route path='/update'>
          <span> </span>
          <h2> BOOK </h2>
          <PutBookForm/>
          <h2> AUTHOR </h2>
          <PutAuthorForm/>
        </Route>
        <Route path='/scrape'>
          <span> </span>
          <h2> BOOK </h2>
          <ScrapeBookForm/>
          <h2> AUTHOR </h2>
          <ScrapeAuthorForm/>
        </Route>
        <Route path='/vis/top-books'>
          <span> </span>
          <VisBookForm/>
        </Route>
        <Route path='/vis/top-authors'>
          <span> </span>
          <VisAuthorForm/>
        </Route>
      </Switch>
   </div>
  );
 }
 

export default App;
