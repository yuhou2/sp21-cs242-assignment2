import os
import ssl
import pymongo
from dotenv import load_dotenv
load_dotenv()
ssl._create_default_https_context = ssl._create_unverified_context


DB_PASSWORD = os.getenv('PASSWORD')

MONGODBPRE = "mongodb+srv://YunesHou:"
MONGODBNAME = "@cluster0.zwdte.mongodb.net/goodreads?retryWrites=true&w=majority"
CLIENT = pymongo.MongoClient(
    MONGODBPRE + DB_PASSWORD + MONGODBNAME,
    ssl_cert_reqs=ssl.CERT_NONE)
DB = CLIENT.goodreads
AUTHOR_COLLECTION = DB["author"]
BOOK_COLLECTION = DB["book"]


def convert_author():
    """convert attribute from author rating, rating_count and review_count to int"""
    cur_author = AUTHOR_COLLECTION.find({})
    for ele in cur_author:
        ele['rating'] = float(ele['rating'])
        # ele['rating_count'] = int(ele['rating_count'])
        # ele['review_count'] = int(ele['review_count'])
        AUTHOR_COLLECTION.save(ele)


def convert_book():
    """convert attribute from book rating, rating_count and review_count to int"""
    cur_book = BOOK_COLLECTION.find({})
    for ele in cur_book:
        ele['rating'] = float(ele['rating'])
        ele['rating_count'] = int(ele['rating_count'])
        ele['review_count'] = int(ele['review_count'])
        BOOK_COLLECTION.save(ele)


def main():
    convert_book()
    convert_author()


if __name__ == "__main__":
    main()